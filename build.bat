@echo off

if not exist .\build mkdir .\build
pushd build

set CompileFlags= /W4 /analyze /Zi /EHsc /std:c++17

cl ../src/main.cpp %CompileFlags%

popd
